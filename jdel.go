/* Автоматизация удаления файлов с заданием остаточного периода */
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"syscall"
	"time"
)

var countDays int
var directory string
var run bool

func init() {
	flag.IntVar(&countDays, "days", 7, "За сколько дней оставлять файлы. Задается цифрой.")
	flag.StringVar(&directory, "dir", "./", "Директория в которой производится удаление файлов.")
	flag.BoolVar(&run, "runme", false, "Программа запускается только с флагом true. Защита от дурака.")
}

func main() {

	flag.Parse()

	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Fatal("Проблема с директорией: ", err)
	}

	fmt.Println("Файлов найдено: ", len(files))

	if !run {
		fmt.Println("Программа запущена в режиме теста. Файлы удаляться не будут. Задайте флаг '-runme true'")
		syscall.Exit(0)
	}

	var delDay time.Time
	delDay = time.Now().AddDate(0, 0, -countDays)
	fmt.Println(delDay)

	for _, file := range files {

		if file.ModTime().Before(delDay) && !file.IsDir() {
			os.Remove(filepath.Join(directory, file.Name()))
		}
	}

}
